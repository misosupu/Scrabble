# (Python) Scrabble

Scrabble is a word game in which two to four players score points by placing tiles, each bearing a single letter, onto a gameboard which is divided into a 15×15 grid of squares. The tiles must form words which, in crossword fashion, flow left to right in rows or downwards in columns. The words are defined in a standard dictionary.

#### Planned features

 * implement AI bot ('possibly maybe')
 * option for multiplayer

#### Dictionary

The dictionary ("words.txt") used for this project is [Enable (**E**nhanced **N**orth **A**merican **B**enchmark **L**exicon)](https://code.google.com/p/dotnetperls-controls/downloads/detail?name=enable1.txt). 

In order to speed up word spelling verfication and search, the above dictionary is stored (*squeezed*) in a [Trie data structure](http://en.wikipedia.org/wiki/Trie).

**TL;DR from Wikipedia**:
*In computer science, a trie, also called digital tree and sometimes radix tree or prefix tree (as they can be searched by prefixes), is an ordered tree data structure that is used to store a dynamic set or associative array where the keys are usually strings.*

The Validator class stores the above-mentioned data structure and performs the validation checks (such as verification if a string is a valid English word etc). 

#### Short-listed features 
###### (which will probably not be implemented in the foreseeable future) 

* GUI using Python OpenGL


### Sponsored by:

 * [Google](http://stackoverflow.com/questions/tagged/python) 
 * [StackOverflow](http://codemirror.net/) -- for noob python questions
 * [Imgur](http://imgur.com/) -- for soothing cat pictures when the code doesn't work
 * [Reddit](http://www.reddit.com/r/shittyprogramming) -- for inspiration
