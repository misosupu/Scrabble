import re
import socket
import urllib.request
import urllib.error
from time import sleep


HTTP_TIMEOUT = 5
MAX_HTTP_RETRIES = 3


class DictionaryHelper(object):
    @staticmethod
    def extract_charset(page):
        m = re.search(b'<meta.*?charset=([^"\']+)', page)
        if m:
            return m.group(1)
        else:
            return 'Could not identify charset.'

    @staticmethod
    def get_definition(word):
        socket.setdefaulttimeout(HTTP_TIMEOUT)
        url = 'http://www.merriam-webster.com/dictionary/' + word
        attempt = 0
        flag_404 = flag_500 = False

        while attempt < MAX_HTTP_RETRIES:
            try:
                with urllib.request.urlopen(url) as response:
                    # content = response.read().decode('utf8')
                    content = response.read()
                    charset = DictionaryHelper.extract_charset(content).decode()
                    # print("The encoding is: {}.".format(charset))
                    content.decode(charset)
            except urllib.error.HTTPError as e:
                if e.code == 404:
                    flag_404 = True
                    break
                elif e.code == 500:
                    flag_500 = True
                    sleep(5)
                    break
            except urllib.error.URLError:
                continue
            else:
                return DictionaryHelper.extract_definition(content.decode())

        error_message = "\n\"Nonetheless, trust the computer " \
                        "- the legibility of the word should not be doubted!\"\n~ Oscar Wilde"
        if flag_404 is True:
            return "Dictionary web page \"{}\" was not found.".format(url) + error_message
        elif flag_500 is True:
            return "Internal Server of \"http://www.merriam-webster.com/\" error." + error_message
        else:
            return "Could not find a definition for the word \"{}\".".format(word) + error_message

    @staticmethod
    def extract_definition(page):
        # TODO: girl, please - fix this nasty regex, asap
        m = re.search(r'(<meta name="Description" content=")(.*?)(" />)', page)
        if m:
            return m.group(2).split('—', 1)[0]
        else:
            return 'Undefined.'


# helper = DictionaryHelper()
# print(helper.get_definition('python'))
# print(helper.get_definition('fdsajfsla'))
