_end = '_end_'


class Trie(object):
    def __init__(self, *words):
        self.root = dict()
        for word in words:
            current_dict = self.root
            for letter in word:
                current_dict = current_dict.setdefault(letter, {})
            current_dict.setdefault(_end, _end)

    def in_trie(self, word):
        # return True
        current_dict = self.root
        for letter in word:
            if letter in current_dict:
                current_dict = current_dict[letter]
            else:
                return False
        else:
            if _end in current_dict:
                return True
            else:
                return False

    def print_trie(self):
        print(self.root)

