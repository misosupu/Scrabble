from Model.cells import *


NUMBER_OF_ROWS = 15
NUMBER_OF_COLUMNS = 15


class Board(object):
    """ Facilitates storage of current game (tile) layout. """

    __triple_word_indexes = [(i, j) for i in range(0, NUMBER_OF_ROWS, 7) for j in range(0, NUMBER_OF_COLUMNS, 7)]
    __double_word_indexes = [(1, 1), (2, 2), (3, 3), (4, 4),
                             (1, 13), (2, 12), (3, 11), (4, 10),
                             (13, 1), (12, 2), (11, 3), (10, 4),
                             (14, 13), (13, 12), (12, 11), (11, 10)]
    __triple_letter_indexes = [(5, 1), (9, 1), (2, 5), (6, 5), (10, 5), (13, 5),
                               (2, 10), (6, 10), (10, 10), (13, 10),
                               (13, 1), (13, 1)]
    __double_letter_indexes = [(0, 3), (0, 11), (2, 6), (2, 8),
                               (3, 0), (3, 7), (3, 14),
                               (6, 2), (6, 6), (6, 8), (6, 12),
                               (7, 3), (7, 11),
                               (8, 2), (8, 6), (8, 8), (8, 12),
                               (11, 0), (11, 7), (11, 14),
                               (14, 3), (14, 11), (14, 6), (14, 8)]

    def __init__(self):
        # every element is an ordinary cell:
        self.grid = [[Cell() for _ in range(NUMBER_OF_ROWS)] for _ in range(NUMBER_OF_COLUMNS)]

        # placing special cells:
        for index in Board.__triple_word_indexes:
            self.grid[index[0]][index[1]] = TripleWordCell()
        for index in Board.__double_word_indexes:
            self.grid[index[0]][index[1]] = DoubleWordCell()
        for index in Board.__triple_letter_indexes:
            self.grid[index[0]][index[1]] = TripleLetterCell()
        for index in Board.__double_letter_indexes:
            self.grid[index[0]][index[1]] = DoubleLetterCell()

    def __len__(self):
        """ Return dimensions of the board. The Scrabble® board is nxn. """
        return len(self.grid)

    def is_empty(self):
        """ Validates whether a word has been played (hence - written) on the board. """
        return self.grid[7][7].is_empty

    def get_cell(self, row, column):
        """ Returns the cell corresponding to (row, column). """
        return self.grid[row][column]

    def place_tiles(self, start, end, tiles):
        """
        Variables start and end must be formatted as follows:
        start = '[A-O]:[1-15]'
        start = [letter, index], end = [letter, index]
        """

        # filled_cells = []
        if start[0] is end[0]:  # eg. vertically
            k = 0

            for i in range(int(start[1]) - 1, int(end[1])):
                self.grid[i][ord(start[0]) - 65].place_tile(tiles[k])
                # filled_cells.append((i, ord(start[0]) - 65, tiles[k], 'v'))
                k += 1
        else:  # eg. horizontally
            k = 0
            for i in range(ord(start[0]) - 65, ord(end[0]) - 64):
                self.grid[int(start[1]) - 1][i].place_tile(tiles[k])
                # filled_cells.append((int(start[1]) - 1, i, tiles[k], 'h'))
                k += 1

    def place_word(self, coordinates, word):
        for index, coords in enumerate(coordinates):
            self.grid[coords[0]][coords[1]].place_tile(word[index])

    def range_is_free(self, coordinates):
        """ Returns the set of cells in range "coordinates,
            which have already been taken during a previous turn. """

        taken = set()
        for (i, j) in coordinates:
            if self.get_cell(i, j).is_empty is False:
                taken.add((i, j))
        return taken

    def __get_horizontal_word(self, row, column, letter):
        """ Returns a horizontal word (if such exists) which intersects the cell (row, column). """

        # letter variable hold the (hypothetical) letter at (row, column)
        word = letter
        # go right
        i = column + 1
        while not self.grid[row][i].is_empty and i <= NUMBER_OF_COLUMNS - 1:
            word += self.grid[row][i].holds_letter
            i += 1
        # go left
        i = column - 1
        while not self.grid[row][i].is_empty and i >= 0:
            word = self.grid[row][i].holds_letter + word
            i -= 1

        if len(word) > 1:
            # return word
            i += 1  # because the last check failed and i got decremented beforehand
            return word, (row, i)  # where (i, j) is the index of the first letter of the word
        else:
            return None

    def __get_vertical_word(self, row, column, letter):
        """ Returns a vertical word (if such exists) which intersects the cell (row, column). """

        # letter variable holds the (hypothetical) letter at (row, column)
        word = letter
        # go down
        i = row + 1
        while not self.grid[i][column].is_empty and i <= NUMBER_OF_ROWS:
            word += self.grid[i][column].holds_letter
            i += 1
        # go up
        i = row - 1
        while not self.grid[i][column].is_empty and i >= 0:
            word = self.grid[i][column].holds_letter + word
            i -= 1

        if len(word) > 1:
            # return word
            i += 1  # because i was incremented and the check returned False
            return word, (i, column)  # where (i, j) is the index of the first letter of the word
        else:
            return None

    def get_intersecting_words(self, layout, full_coordinates, partial_coordinates, word):
        """
        :param layout: Either 'h' (horizontal) or 'v' (vertical).
        :param partial_coordinates (set): Coordinates range on board for word.
        :param word:
        :return: Intersecting_words is in the form [(word, (i_start, j_start)].
        """

        intersecting_words = []

        for (i, j) in partial_coordinates:
            # if we write a word vertically - check horizontal word crosses
            # else - check vertical word crosses
            index = full_coordinates.index((i, j))  # holds index of letter in word, corresponding to (i, j) on board

            if layout == 'v':
                result = self.__get_horizontal_word(i, j, word[index])
                if result is not None:  # => result = (word, start_cell_indexes)
                    horizontal_word = result[0]
                    start_cell = result[1]
                    end_cell = (start_cell[0], start_cell[1] + len(horizontal_word) - 1)
                    intersecting_words.append((horizontal_word, start_cell, end_cell))
            else:  # eg - layout == 'h'
                result = self.__get_vertical_word(i, j, word[index])
                if result is not None:
                    vertical_word = result[0]
                    start_cell = result[1]
                    end_cell = (start_cell[0] + len(vertical_word) - 1, start_cell[1])
                    intersecting_words.append((vertical_word, start_cell, end_cell))

        return intersecting_words
