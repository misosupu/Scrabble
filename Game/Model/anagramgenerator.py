import itertools


class AnagramGenerator:
    """ Generates string permutations based on a certain player's rack. """

    @staticmethod
    def __sort_by_type(letters):
        """ Distributes the vowels and consonants in "letters"  into letters_sorted.
            The first list holds the vowels, the second one - the consonants. """

        vowels = ['a', 'e', 'i', 'o', 'u']
        letters_sorted = [[], []]
        for letter in letters:
            if letter in vowels:
                letters_sorted[0].append(letter)
            else:
                letters_sorted[1].append(letter)

        return letters_sorted

    @staticmethod
    def __get_n_letter_words(sorted_letters, length, min_vowels, max_vowels):
        """ Returns all n-length variations of sorted_letters, based on a (rather lame) heuristic:
                Words must have no more than 4 consecutive consonants.
                Words will be stored in dict {key = word len: [word1, word2, .., wordk], ... }. """

        def find_subsets(s, size):
            """ Returns all possible subsets of s of size "size". """
            return set(itertools.combinations(s, size))

        n_permutations = dict()
        n_permutations[length] = []  # initialize empty list to hold the "length" letter words
        if len(sorted_letters[0]) >= min_vowels:  # if the number of vowels satisfies the defined heuristics
            for num_vowels in range(min_vowels, max_vowels + 1):
                for vowels_subset in find_subsets(sorted_letters[0], num_vowels):
                    for consonant_subset in find_subsets(sorted_letters[1], length - num_vowels):
                        l = list(vowels_subset) + list(consonant_subset)  # TODO: obviously set doesn't allow multiple same elements. Fail.
                        n_permutations[length] += set(itertools.permutations(l))

        return n_permutations

    @staticmethod
    def create_permutations(letters):
        """ Generates permutations based on the letters list, respectively for 7-letter
            words, 6-letters words and so on. There is no such thing as a one-letter word."""

        sorted_letters = AnagramGenerator.__sort_by_type(letters)

        permutations = {}
        permutations.update(AnagramGenerator.__get_n_letter_words(sorted_letters, 7, 2, 4))
        permutations.update(AnagramGenerator.__get_n_letter_words(sorted_letters, 6, 2, 4))
        permutations.update(AnagramGenerator.__get_n_letter_words(sorted_letters, 5, 1, 3))
        permutations.update(AnagramGenerator.__get_n_letter_words(sorted_letters, 4, 1, 2))
        permutations.update(AnagramGenerator.__get_n_letter_words(sorted_letters, 3, 1, 2))
        permutations.update(AnagramGenerator.__get_n_letter_words(sorted_letters, 2, 1, 2))

        return permutations
