class Cell(object):
    def __init__(self):
        self.type = "+"
        self.is_empty = True
        self.holds_letter = ''
        self.multiply_by = 1

    def place_tile(self, tile):
        self.is_empty = False
        self.holds_letter = tile

    def __str__(self):
        if self.is_empty is True:
            return self.type
        else:
            return self.holds_letter

    def __len__(self):
        if self.is_empty is True:
            return len(self.type)
        else:
            return len(self.holds_letter)


class TripleWordCell(Cell):
    def __init__(self):
        super().__init__()
        self.type = "TW"


class DoubleWordCell(Cell):
    def __init__(self):
        super().__init__()
        self.type = "DW"


class TripleLetterCell(Cell):
    def __init__(self):
        super().__init__()
        self.type = "TL"
        self.multiply_by = 3


class DoubleLetterCell(Cell):
    def __init__(self):
        super().__init__()
        self.type = "DL"
        self.multiply_by = 2
