from Model.tileholder import *


class Player(TileHolder):
    """ Attributes:
        name = player name
        letters = list of the 7 letters the player currently has
        points = players score
    """

    def __init__(self, name, bag):
        self.name = name
        self.points = 0
        TileHolder.__init__(self, bag)

    def __str__(self):
        return "Player {} has {} points so far.".format(self.name, self.points)

    def update_points(self, new_score):
        self.points += new_score

    def withdraw(self):
        pass

