from Model.tileholder import *


class Computer(TileHolder):
    def __init__(self, bag, name="Computer"):
        self.name = name
        self.points = 0
        self.consecutive_passes = 0
        TileHolder.__init__(self, bag)

    def __str__(self):
        return "Computer has {} points so far.".format(self.points)

    def update_points(self, new_score):
        self.points += new_score

    def can_pass(self):
        """ Function defining whether the computer should pass a turn or not.
            Obviously, more than two consecutive passes is not a reasonable turn. """

        if self.consecutive_passes < 2:
            return True
        return False

    def play(self, board, inspector, generator, validator):
        import itertools
        # step 1: pass the computer rack to the anagram generator:
        # get list of all possible permutations:
        string_permutations = generator.create_permutations([letter for (letter, score) in self.tiles])
        # step 2: sift the real words from above list
        words = filter(validator.validate_word, list(itertools.chain.from_iterable(string_permutations.values())))
        for k in range(0, 5):
            print("I have found a word!: {}".format(next(words)))

        # step 3: obtain sorted by priority list of slots
        prioritized_slots = inspector.prioritize_options(board)
        print("Possible move slots: {}".format(prioritized_slots))

        # step 4: start "play"-ing words in slots by order of priority
        for slot in prioritized_slots:
            inspector.test_slot(slot, board, string_permutations, validator)
