try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Scrabble Project',
    'author': 'Martina Tonkovska',
    'url': 'https://gitlab.com/misosupu/Scrabble',
    'download_url': 'https://gitlab.com/misosupu/Scrabble.git',
    'author_email': 'martinatonkovska@gmail.com',
    'version': '0.1',
    'install_requires': [],
    'packages': ['Game', 'Tests', 'Resources'],
    'scripts': [],
    'name': 'scrabble'
}

setup(**config), 
