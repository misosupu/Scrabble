import unittest
from Game.Model.player import Player
from Game.Model.bag import Bag


class TestPlayer(unittest.TestCase):
    def setUp(self):
        self.bag = Bag()

    def test_initialize(self):
        p = Player("Player1", self.bag)
        self.assertIsInstance(p, Player)
        self.assertEqual(p.name, "Player1")
        self.assertEqual(p.points, 0)

    def test_updating_points(self):
        p = Player("Player1", self.bag)  # player has 0 points so far
        p.update_points(100)
        self.assertEqual(p.points, 100)


if __name__ == '__main__':
    unittest.main()
