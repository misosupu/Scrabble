import unittest
from Game.Model.bag import Bag


class BagTest(unittest.TestCase):
    def setUp(self):
        self.bag = Bag()

    def test_get_letter_score(self):
        """ Tests case whether a newly initialized bag contains the appropriate initial values. """
        self.assertEqual(self.bag.get_letter_score('n'), 1)
        self.assertEqual(self.bag.get_letter_score('a'), 1)
        self.assertEqual(self.bag.get_letter_score('y'), 4)
        self.assertEqual(self.bag.get_letter_score('x'), 8)
        self.assertEqual(self.bag.get_letter_score('a'), 1)

    def test_draw_tiles(self):
        """ Test case whether drawing a tile decrements overall quantity in bag. """
        # letters = list(map(chr, range(97, 123)))

        old_quantity = self.bag.available_tiles
        self.bag.draw_tiles(3)
        self.assertEqual(old_quantity, self.bag.available_tiles + 3), "Testing of the draw_tiles function in class Bag"\
                                                                          "has failed."

    def test_return_tiles(self):
        """ Test case whether returning a tile increments its quantity in bag. """

        letters_to_return = ['a', 'c', 'e', 'z']
        quantities_before = [attributes[1] for attributes in [self.bag.bag[letter] for letter in letters_to_return]]
        self.bag.return_tiles(letters_to_return)
        quantities_after = [attributes[1] for attributes in [self.bag.bag[letter] for letter in letters_to_return]]
        for i in range(0, len(quantities_after)):
            self.assertEqual(quantities_before[i] + 1, quantities_after[i]), "Testing of the return_tiles function in class Bag"\
                                                                             "has failed."

    def test_is_empty(self):
        # Newly created bag should not be empty:
        self.assertFalse(self.bag.is_empty())
        # Draw all the tiles in the bag:
        self.bag.draw_tiles(self.bag.available_tiles)
        # Bag should now be empty:
        self.assertTrue(self.bag.is_empty())


if __name__ == '__main__':
    unittest.main()
