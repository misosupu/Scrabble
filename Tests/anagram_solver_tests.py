import unittest
from Game.Model.anagramgenerator import AnagramGenerator


class TestAnagramSolver(unittest.TestCase):
    def setUp(self):
        self.generator = AnagramGenerator()

    def test_sorting_by_type(self):
        letters = ['a', 'z', 'e', 't', 'y', 's']
        sorted_by_type = [['a', 'e'], ['z', 't', 'y', 's']]
        result = self.generator.sort_by_type(letters)
        self.assertEqual(sorted_by_type, result)


if __name__ == '__main__':
    unittest.main()
