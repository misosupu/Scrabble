import unittest
from Game.Model.cells import *


class TestCells(unittest.TestCase):
    def setUp(self):
        self.cell = Cell()

    def test_initialization(self):
        self.assertTrue(self.cell.is_empty)
        self.assertEqual(self.cell.multiply_by, 1)
        self.assertEqual(self.cell.holds_letter, '')

    def test_place_tile(self):
        self.cell.place_tile('z')
        self.assertFalse(self.cell.is_empty)
        self.assertEqual(self.cell.holds_letter, 'z')


if __name__ == '__main__':
    unittest.main()
