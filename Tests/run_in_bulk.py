import unittest

test_modules = [
    'Tests.bag_tests',
    'Tests.board_tests',
    'Tests.cells_tests',
    'Tests.history_tests',
    'Tests.player_tests',
    'Tests.tileholder_tests',
    'Tests.validator_tests'
    ]

suite = unittest.TestSuite()

for t in test_modules:
    try:
        # If the module defines a suite() function, call it to get the suite.
        mod = __import__(t, globals(), locals(), ['suite'])
        suitefn = getattr(mod, 'suite')
        suite.addTest(suitefn())
    except (ImportError, AttributeError):
        # else, just load all the test cases from the module.
        suite.addTest(unittest.defaultTestLoader.loadTestsFromName(t))

if __name__ == '__main__':
    unittest.TextTestRunner().run(suite)
