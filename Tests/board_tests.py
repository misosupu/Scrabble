import unittest
from Game.Model.board import Board


class TestBoard(unittest.TestCase):
    def setUp(self):
        """
        The board will have the following structure:
                * * * * * * * * *
                * c             *
                * a             *
                * t h i s       *
                *       n       *
                *       a       *
                *       k       *
                *     z e b r a *
                * * * * * * * * *
        """
        self.board = Board()
        tiles = ['t', 'h', 'i', 's']
        self.board.place_tiles(('G', '8'), ('J', '8'), tiles)
        tiles = ['c', 'a']  # 'cat' vertically
        self.board.place_tiles(('G', '6'), ('G', '7'), tiles)
        tiles = ['n', 'a', 'k', 'e']  # "snake" vertically
        self.board.place_tiles(('J', '9'), ('J', '12'), tiles)
        tiles = ['z', 'e', 'b', 'r', 'a']  # "zebra" horizontally
        self.board.place_tiles(('I', '12'), ('M', '12'), tiles)

    def test_is_empty(self):
        """ Test if newly created board is empty. """
        b = Board()
        self.assertTrue(b.is_empty())

    def test_get_cell(self):
        self.assertEqual(self.board.get_cell(7, 6).holds_letter, 't')
        self.assertEqual(self.board.get_cell(7, 7).holds_letter, 'h')
        self.assertEqual(self.board.get_cell(7, 8).holds_letter, 'i')
        self.assertEqual(self.board.get_cell(7, 9).holds_letter, 's')

    def test_place_horizontal_word(self):
        # check if placing a horizontal word works correctly
        self.assertEqual(self.board.grid[7][6].holds_letter, 't')
        self.assertEqual(self.board.grid[7][7].holds_letter, 'h')
        self.assertEqual(self.board.grid[7][8].holds_letter, 'i')
        self.assertEqual(self.board.grid[7][9].holds_letter, 's')

    def test_place_vertical_word(self):
        # check if placing a vertical word works correctly
        self.assertEqual(self.board.grid[5][6].holds_letter, 'c')
        self.assertEqual(self.board.grid[6][6].holds_letter, 'a')
        self.assertEqual(self.board.grid[7][6].holds_letter, 't')

    def test_if_range_is_free(self):
        coordinates = [(0, 0), (6, 7), (3, 7), (14, 14)]
        for row, col in coordinates:
            self.assertTrue(self.board.grid[row][col].is_empty)

    def test_place_tiles_out_of_range(self):
        # check if word intersecting with invalid range is placed
        tiles = ['s', 't', 'r', 'a', 'n', 'g', 'e']
        self.board.place_tiles(('K', '4'), ('O', '4'), tiles)
        coordinates = [(3, 10), (3, 11), (3, 12), (3, 13), (3, 14)]
        for row, col in coordinates:
            self.assertFalse(self.board.grid[row][col].is_empty)

    def test_get_vertical_word(self):
        result = ("cat", (5, 6))
        self.assertEqual(self.board._Board__get_vertical_word(6, 6, 'a'), result)

    def test_get_horizontal_word(self):
        result = ("zebra", (11, 8))
        self.assertEqual(self.board._Board__get_horizontal_word(11, 10, 'b'), result)

    def test_get_intersections(self):
        pass


if __name__ == '__main__':
    unittest.main()
