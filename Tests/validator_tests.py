import unittest
from Game.Validators.validator import Validator
from Game.Model.board import Board
from Game.Model.history import History
from Game.Model.player import Player
from Game.Model.bag import Bag


class TestValidator(unittest.TestCase):
    def setUp(self):
        self.validator = Validator()

    def test_word_verification(self):
        real_words = ["obscure", "hilarious", "epiphany", "ubiquitous", "ledge"]
        for word in real_words:
            self.assertTrue(self.validator.validate_word(word))

        false_words = ["opiphany", "obscur", "hilarias", "lege"]
        for word in false_words:
            self.assertFalse(self.validator.validate_word(word))

    def test_index_validation(self):
        incorrect_indexes = ["G:16", "Z:9", "a:7"]
        for index in incorrect_indexes:
            self.assertFalse(self.validator.validate_index(index))

        correct_indexes = ["G:8", "A:4", "N:14", "J:1"]
        for index in correct_indexes:
            self.assertTrue(self.validator.validate_index(index))

    def test_check_duplicate_word(self):
        history = History()
        history.document_turn(2, "random", "play", "ubiquitous", 10)
        duplicate_word = "ubiquitous"
        unmatching_word = "epiphany"
        self.assertFalse(self.validator.check_duplicate_word(duplicate_word, history))
        self.assertTrue(self.validator.check_duplicate_word(unmatching_word, history))

    def test_validate_word_length(self):
        test_words = ["obscure", "fail"]
        coordinates = [(7, 2), (7, 3), (7, 4), (7, 5), (7, 6), (7, 7), (7, 8)]
        self.assertTrue(self.validator.validate_word_length(test_words[0], coordinates))
        self.assertFalse(self.validator.validate_word_length(test_words[1], coordinates))

    def test_validate_unique_name(self):
        bag = Bag()
        active_players = [Player("Random1", bag), Player("Random2", bag), Player("Random3", bag)]
        self.assertTrue(self.validator.validate_unique_name("Random4", active_players))
        self.assertFalse(self.validator.validate_unique_name("Random3", active_players))

    def test_validate_index_correctness(self):
        self.assertTrue(self.validator.validate_index_correctness(['G', '8'], ['I', '8'], 'h'))
        self.assertTrue(self.validator.validate_index_correctness(['A', '2'], ['A', '7'], 'v'))

        self.assertFalse(self.validator.validate_index_correctness(['g', '8'], ['I', '8'], 'h'))
        self.assertFalse(self.validator.validate_index_correctness(['A', '7'], ['A', '2'], 'h'))


if __name__ == '__main__':
    unittest.main()
